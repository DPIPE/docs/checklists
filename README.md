# Checklists

This repository is a collection of checklist templates to create issues from.

## Create new templates

Place a markdown file in the [templates directory](./.gitlab/issue_templates/). More information is available in the [GitLab docs](https://docs.gitlab.com/ee/user/project/description_templates.html). Check boxes are inserted with `[ ]`.

## Create new issues from templates

Create a new issue from within the repository as you would normally. A drop-down menu should appear in the issue editing view containing all available templates. Select the one you want to create an issue from and fill in the rest.

## Available templates

### [Production shift](./.gitlab/issue_templates/production_shift.md)

Use this template to create production duty shift issues.

### [TSD downtime preparation](./.gitlab/issue_templates/tsd-downtime-preparation.md)

Use this template to create scheduled TSD downtime issues (checklist before TSD downtime).

### [restart production](./.gitlab/issue_templates/restart-production.md)

Use this template to create issues for checking whether all production scripts run as expected (after production duty shift or TSD/NSC downtime).