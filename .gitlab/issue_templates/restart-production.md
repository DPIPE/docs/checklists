# Service checklist after TSD VMs reboot

In order to start the services listed below, you will need to [log in as service user](https://dpipe.gitlab.io/docs/docs-pages/docs-store/docs/Production-routines/Use-of-service-user/#how-to-log-in) to the corresponding VMs.

Check the status of the following services and, if necessary, start them (see linked information). **Begin with ELLA, anno and Dagu**, as no new results can be answered out before the analyses are available for interpretation. Even if only TSD is down, **check whether the nsc-exporter is running**::

**TSD**
- [ ] [ELLA](https://dpipe.gitlab.io/docs/docs-pages/docs-store/docs/Production-routines/ELLA-core-production/#supervisor-process-manager)

- [ ] [Anno](https://dpipe.gitlab.io/docs/docs-pages/docs-store/docs/Production-routines/Running-anno/#supervisor-process-manager)

- [ ] [Dagu](https://dpipe.gitlab.io/docs/docs-pages/docs-store/docs/Production-routines/Scheduled-recurring-jobs/#stoppingstarting-service)

- [ ] [Filelock exporter](https://dpipe.gitlab.io/docs/docs-pages/docs-store/docs/Production-routines/Execution-and-monitoring-of-pipeline/#startingstopping-production-services)

- [ ] [Production executor](https://dpipe.gitlab.io/docs/docs-pages/docs-store/docs/Production-routines/Execution-and-monitoring-of-pipeline/#startingstopping-production-services)

- [ ] [Production webUI](https://dpipe.gitlab.io/docs/docs-pages/docs-store/docs/Production-routines/Execution-and-monitoring-of-pipeline/#startingstopping-production-services)

**NSC**
- [ ] [nsc-exporter](https://dpipe.gitlab.io/docs/docs-pages/docs-store/docs/Production-routines/Execution-and-monitoring-of-pipeline/#startingstopping-production-services). After TSD downtime: check log (<code>/boston/diag/transfer/sw/logsAPI/nsc-exporter-YYYY-MM-DD.log</code> or <code>/ess/p22/data/durable/s3-api/production/logs/nsc-exporter/nsc-exporter-YYYY-MM-DD.log</code>) to see whether the traffic is still OK. If not, contact TSD (tsd-p22-help@usit.uio.no)

- [ ] [Production lims exporter api](https://dpipe.gitlab.io/docs/docs-pages/docs-store/docs/Production-routines/Execution-and-monitoring-of-pipeline/#startingstopping-production-services)

- [ ] [Production executor](https://dpipe.gitlab.io/docs/docs-pages/docs-store/docs/Production-routines/Execution-and-monitoring-of-pipeline/#startingstopping-production-services)

- [ ] [Production webUI](https://dpipe.gitlab.io/docs/docs-pages/docs-store/docs/Production-routines/Execution-and-monitoring-of-pipeline/#startingstopping-production-services)

> **Tip**: TSD/NSC downtime can cause many samples to fail the pipelines. To resume analysis for several samples at once, change their status directly in the database.
