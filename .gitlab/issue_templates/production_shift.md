The outgoing and the incoming responsibles must go through the following items:
- [ ] Check whether the information in the [OnDuty_production](https://gitlab.com/ousamg/docs/wiki/-/wikis/production/production-duty) is updated.
- [ ] Check whether the Clarity “Manager Review” is empty. If not, specify responsibilities further.
  - _COMMENT_:
- [ ] Check whether the Clarity *lims-exporter process* step is empty. If not, specify responsibilities further.
  - _COMMENT_:
- [ ] Check whether the Clarity *bioinformatic process* step is empty. If not, specify responsibilities further.
  - _COMMENT_:
- [ ] On NSC: check whether the cleaning white list `/boston/diag/transfer/sw/clean_whitelist.txt` and black list `/boston/diag/transfer/sw/clean_blacklist.txt` are empty. If not, specify why and what consequences that has on cleaning up the areas below.
  - _COMMENT_:
- [ ] On TSD: check whether the cleaning white list `/ess/p22/data/durable/production/sw/clean_whitelist.txt` and black list `/ess/p22/data/durable/production/sw/clean_blacklist.txt` are empty. If not, specify why and what consequences that has on cleaning up the areas below.
  - _COMMENT_:
- [ ] Check whether the NSC production (`/boston/diag/production/data/{samples,analyses-work,analyses-results/{singles,trios}}`), transfer (`/boston/diag/transfer/production/{normal,high,urgent}/{samples,analyses-work,analyses-results/{singles,trios},ella-incoming}`) and transferred (`/boston/diag/transfer/production/transferred/{normal,high,urgent}/{samples,analyses-work,analyses-results/{singles,trios},ella-incoming}`) areas are empty. If not, specify responsibilities further.
  - _COMMENT_:
- [ ] Check whether the TSD production (`/ess/p22/data/durable/production/data/analyses-work`) and transfer areas (`/ess/p22/data/durable/s3-api/production/{normal,high,urgent}`) are empty. If not, specify responsibilities further.
  - _COMMENT_:
- [ ] Check whether all analyses marked with [any variations of] "SLETTES" have been deleted from the ELLA database (the `ella-imported` folders can be kept).
  - _COMMENT_:
- [ ] Go through all open Helpdesk issues and agree on responsibility.
  - _COMMENT_:
- [ ] Stop `nsc-exporter` on NSC. If not, specify why and what is the further plan.


Upon taking over operations, the incoming responsible further makes sure to:
- [ ] Start `nsc-exporter` on NSC and go through the [operations checklist](https://gitlab.com/DPIPE/docs/checklists/-/blob/main/.gitlab/issue_templates/restart-production.md).

/label ~prod-duty-shift