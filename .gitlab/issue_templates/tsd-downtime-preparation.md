# Service checklist before scheduled TSD downtime

Make sure to stop the services listed below before a scheduled TSD downtime. Stopping them properly (as opposed to them crashing during the downtime) will facilitate the restart of the services when TSD is back up. 

In order to stop the services listed below, you will need to [log in as service user](https://dpipe.gitlab.io/docs/docs-pages/docs-store/docs/Production-routines/Use-of-service-user/#how-to-log-in) to the corresponding VMs.

### Production scripts

- [ ] [nsc-exporter](https://dpipe.gitlab.io/docs/docs-pages/docs-store/docs/Production-routines/Execution-and-monitoring-of-pipeline/#1-stopping-lims-exporter-and-nsc-exporter-after-confirming-that-the-processes-are-sleeping)

- [ ] [Filelock exporter](https://dpipe.gitlab.io/docs/docs-pages/docs-store/docs/Production-routines/Execution-and-monitoring-of-pipeline/#2-stopping-filelock-exporter-after-confirming-that-the-processes-are-sleeping)

- [ ] [Production executor](https://dpipe.gitlab.io/docs/docs-pages/docs-store/docs/Production-routines/Execution-and-monitoring-of-pipeline/#3-stopping-webui-executor-and-nsc-sequencing-overview-page-on-tsd-after-confirming-that-the-processes-are-sleeping)  (on TSD only)

- [ ] [Production webUI](https://dpipe.gitlab.io/docs/docs-pages/docs-store/docs/Production-routines/Execution-and-monitoring-of-pipeline/#3-stopping-webui-executor-and-nsc-sequencing-overview-page-on-tsd-after-confirming-that-the-processes-are-sleeping)  (on TSD only)

### Annotation and interpretation

Stop the ELLA and anno processes handled by the respective supervisors and superdupervisors. 

- [ ] [ELLA](https://dpipe.gitlab.io/docs/docs-pages/docs-store/docs/Production-routines/ELLA-core-production/#supervisor-process-manager)

- [ ] [Anno](https://dpipe.gitlab.io/docs/docs-pages/docs-store/docs/Production-routines/Running-anno/#supervisor-process-manager)

- [ ] [Dagu](https://dpipe.gitlab.io/docs/docs-pages/docs-store/docs/Production-routines/Scheduled-recurring-jobs/#stoppingstarting-service)

